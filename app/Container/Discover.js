import React, {Component} from 'react';
import {View, ScrollView,SafeAreaView} from 'react-native';
import TextComponent from '../Components/Text/Text';
import Card from '../Components/Card/Card';
import CardImage from '../Components/CardImage/CardImage';
import Button from '../Components/Button/Button';
import ChannelCard from '../Components/ChannelCard/ChannelCard';
import MenuFilter from '../Components/MenuFilter/MenuFilter';

const recomendedDjJSON = {
  djName: 'Avicii',
  image:
    'https://www.billboard.com/files/media/05-bw-avicii-billboard-1548.jpg',
  title: 'DIGWEED LIKES TO LISTEN TO AVICII',
};

const channelJSON = {
  chanelTitle: 'NEWEST CHANNELS',
  newChannels: [
    {
      name: 'Martin Garrix',
      url:
        'https://www.billboard.com/files/styles/article_main_image/public/media/Martin-Garrix-press-photo-cr-Martin-Beck-2017-billboard-1548.jpg',
    },
    {
      name: 'Alan Walker',
      url:
        'https://i.pinimg.com/474x/0d/a8/66/0da8664e1aab2f43a8e06c1b75cf8323.jpg',
    },
    {
      name: 'Calvin Harris',
      url:
        'https://www.magneticmag.com/.image/t_share/MTM4Mjc0MzM5OTExOTAyNTM0/calvin-harrisjpg.jpg',
    },

    {
      name: 'Marshmellow',
      url: 'https://weraveyou.com/wp-content/uploads/2017/02/mellomain.jpg',
    },
  ],
};

const dataJSON = {
  tabHeader: 'Explore',
  cardsSectionHeader: 'BECAUSE YOU LISTEN RESISTANCE CLOSING PARTY',
  cardRecomendation: [
    {
      image:
        'https://exclaim.ca//images/Underoath-Erase-Me-Cover-Art-Large.jpg',
      actualTime: '1:23:04',
      HeaderText: 'Tomorrowland',
      subHeaderText: 'John Digweed',
      subtTitle1Text: 'Play by: John Digweed',
      subtitle2Text: '2019-07-19, Belgium',
      paragraphText:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua \n Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat \n Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur \n Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
  ],
  cardRelatedHeader: 'LATEST SETS',
  cards: [
    {
      image:
        'https://exclaim.ca//images/Underoath-Erase-Me-Cover-Art-Large.jpg',
      actualTime: '1:23:04',
      HeaderText: 'Tomorrowland',
      subHeaderText: 'John Digweed',
      subtTitle1Text: 'Play by: John Digweed',
      subtitle2Text: '2019-07-19, Belgium',
      paragraphText:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua \n Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat \n Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur \n Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      image: 'https://i.redd.it/n9ytnjdhve011.png',
      actualTime: '1:23:04',
      HeaderText: 'Avicii',
      subHeaderText: 'Avicii',
      subtTitle1Text: 'Play by: Avicii',
      subtitle2Text: '2019-07-19, Belgium',
      paragraphText:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua \n Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat \n Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur \n Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      image:
        'https://upload.wikimedia.org/wikipedia/en/thumb/5/51/My_Way_Calvin_Harris.jpg/220px-My_Way_Calvin_Harris.jpg',
      actualTime: '1:23:04',
      HeaderText: 'My Way',
      subHeaderText: 'Calvin Harris',
      subtTitle1Text: 'Play by: Calvin Harris',
      subtitle2Text: '2019-07-19, Belgium',
      paragraphText:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua \n Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat \n Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur \n Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
  ],
};

class Discover extends Component {
  renderRecomendationCard = () => {
    const cards = dataJSON.cardRecomendation;

    return cards.map(item => {
      return (
        <React.Fragment>
          <Card
            image={item.image}
            actualTime={item.actualTime}
            HeaderText={item.HeaderText}
            subHeaderText={item.subHeaderText}
            paragraphText={item.paragraphText}
            subtTitle1Text={item.subtTitle1Text}
            subtitle2Text={item.subtitle2Text}
            initialValue={true}
          />
          <View style={{borderBottomColor: 'grey', borderBottomWidth: 0.5}} />
        </React.Fragment>
      );
    });
  };

  renderCards = () => {
    const cards = dataJSON.cards;

    return cards.map(item => {
      return (
        <React.Fragment>
          <Card
            image={item.image}
            actualTime={item.actualTime}
            HeaderText={item.HeaderText}
            subHeaderText={item.subHeaderText}
            paragraphText={item.paragraphText}
            subtTitle1Text={item.subtTitle1Text}
            subtitle2Text={item.subtitle2Text}
          />
          <View style={{borderBottomColor: 'grey', borderBottomWidth: 0.5}} />
        </React.Fragment>
      );
    });
  };

  renderNewestChannels = () => {
    const channels = channelJSON.newChannels;

    return channels.map(item => {
      return (
        <View style={{flex: 1, marginRight: 20}}>
          <ChannelCard url={item.url} />
          <TextComponent style={{marginLeft: 20}} value={item.name} />
        </View>
      );
    });
  };

  render() {
    return (
      <SafeAreaView>
        <ScrollView>
          <TextComponent
            value={dataJSON.tabHeader}
            size="xlarge"
            color="white"
            style={{textAlign: 'center', marginBottom: 16}}
          />
          <MenuFilter />
          <View style={{marginBottom: 10}}>
            <TextComponent
              value={channelJSON.chanelTitle}
              style={{marginTop: 40, textAlign: 'center'}}
              bold
              color="white"
              size="small"
            />
          </View>

          <View>
            <ScrollView horizontal={true}>
              {this.renderNewestChannels()}
            </ScrollView>
          </View>
          <View style={{marginBottom: 10}}>
            <TextComponent
              value={recomendedDjJSON.title}
              style={{marginTop: 40, textAlign: 'center'}}
              bold
              color="white"
              size="small"
            />
          </View>
          <CardImage
            url={recomendedDjJSON.image}
            title={recomendedDjJSON.djName}
          />
          <TextComponent
            value={dataJSON.cardsSectionHeader}
            size="small"
            style={{margin: 16, textAlign: 'center'}}
            bold
            color="white"
          />
          {this.renderRecomendationCard()}
          <TextComponent
            value={dataJSON.cardRelatedHeader}
            style={{marginTop: 40, textAlign: 'center'}}
            bold
            color="white"
            size="small"
          />
          {this.renderCards()}
          <Button
            value="Show more"
            style={{
              width: 100,
              backgroundColor: 'transparent',
              borderRadius: 3,
              borderWidth: 1,
              borderColor: '#fff',
              marginBottom: 20,
              height: 30,
              left: 140,
              top: 20,
            }}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default Discover;
