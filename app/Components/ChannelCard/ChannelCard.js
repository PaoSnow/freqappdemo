import React, {Component} from 'react';
import {View} from 'react-native';
import ImageBackground from '../ImageBackground/ImageBackground';
import Button from '../Button/Button';
import Icon from 'react-native-vector-icons/Foundation';

class ChannelCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {title, url} = this.props;

    return (
      <View>
        <ImageBackground
          url={url}
          imageStyle={{ width: 200, height: 200}}
          // textStyle={{top: 250, left: 20}}
          title="">
          <Icon name="burst-new" color="white" size={40} />
          <Button
            style={{
              width: 80,
              backgroundColor: 'transparent',
              borderRadius: 3,
              borderWidth: 1,
              borderColor: '#fff',
              height: 30,
              left: 60,
              top: 100,
            }}
            value="Follow"

          />
        </ImageBackground>
      </View>
    );
  }
}

export default ChannelCard;
