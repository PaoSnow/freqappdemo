import React, {Component} from 'react';
import {View, ImageBackground} from 'react-native';
import Text from '../Text/Text';

class ImageBackgroundComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {url, title, children, textStyle, imageStyle} = this.props;
    return (
      <ImageBackground source={{uri: url}} style={imageStyle}>
        <View style={textStyle}>
          <Text value={title} color="white" bold />
        </View>
        {children}
      </ImageBackground>
    );
  }
}

export default ImageBackgroundComponent;
