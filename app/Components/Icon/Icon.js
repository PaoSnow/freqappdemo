import React, {Component} from 'react';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

class IconComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {size, name, color, style} = this.props;
    return <Icon name={name} size={size} color={color} style={style} />;
  }
}

export default IconComponent;
