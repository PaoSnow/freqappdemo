import {TouchableOpacity, View} from 'react-native';
import React, {Component} from 'react';
import Text from '../Text/Text';

class ButtonComponent extends Component {
  // getFullWidthStyle = () => {
  //   const {isFullWidth} = this.props;
  //   if (isFullWidth) {
  //     return styles.fullWidth;
  //   }
  //   return null;
  // };

  render() {
    const {onPress, style, value} = this.props;

    return (
      <TouchableOpacity onPress={onPress} style={style}>
        {/* <View style={{flex: 1, alignItems:'center'}}> */}
        <Text
          value={value}
          color="white"
          style={{flex: 1,marginTop: 4, alignContent: 'center', textAlign: 'center'}}
        />
        {/* </View> */}
      </TouchableOpacity>
    );
  }
}

export default ButtonComponent;
