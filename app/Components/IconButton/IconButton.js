import React, {Component} from 'react';
import {View, TouchableOpacity} from 'react-native';
import IconComponent from '../Icon/Icon';
class IconButton extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {size, name, color, onPress} = this.props;
    return (
      <TouchableOpacity onPress={onPress}>
        <IconComponent name={name} size={size} color={color} />
      </TouchableOpacity>
    );
  }
}

export default IconButton;
