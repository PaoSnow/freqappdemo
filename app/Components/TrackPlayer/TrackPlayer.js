import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import TrackButton from '../TrackButton/TrackButton';
import TextComponent from '../Text/Text';
import ProgressBar from '../ProgressBar/ProgressBar';

class TrackPlayer extends Component {
  render() {
    const {image, currentSongTime, songDuration} = this.props;

    return (
      <View style={styles.container}>
        <TrackButton image={image} />
        <View style={styles.subContainer}>
          <TextComponent
            bold
            color="black"
            size="small"
            value="Resistance Closing Party"
          />
          <ProgressBar />
          <View style={styles.textContainer}>
            <TextComponent
              bold
              color="black"
              style={{fontSize: 10}}
              value={currentSongTime}
            />
            <TextComponent
              color="black"
              style={{fontSize: 10, marginLeft: 5, marginRight: 5}}
              value="|"
            />
            <TextComponent
              color="black"
              style={{fontSize: 10}}
              value={songDuration}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 80,
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 12,
    paddingBottom: 12,
  },

  subContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  textContainer: {
    flexDirection: 'row',
  },
});

export default TrackPlayer;
