import React, {Component} from 'react';
import Lodash from 'lodash';
import PropTypes from 'prop-types';
import {View, TouchableOpacity} from 'react-native';
import Text from '../Text/Text';

class AccordionComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: props.initialValue,
    };
  }

  handleOnHeaderPress = () => {
    this.setState(objCurrentState => {
      return {
        isOpen: !objCurrentState.isOpen,
      };
    });
  };

  renderHeader = () => {
    const {renderHeader} = this.props;

    return renderHeader;
  };

  renderBody = () => {
    const {renderBody} = this.props;
    const {isOpen} = this.state;
    if (!isOpen) {
      return null;
    }

    if (renderBody) {
      return renderBody;
    }
    return null;
  };

  render() {
    return (
      <React.Fragment>
        <TouchableOpacity onPress={() => this.handleOnHeaderPress()}>
          {this.renderHeader()}
        </TouchableOpacity>
        {this.renderBody()}
      </React.Fragment>
    );
  }
}

AccordionComponent.defaultProps = {
  initialValue: false,
};

AccordionComponent.propTypes = {
  initialValue: PropTypes.bool,
  // isOpen: PropTypes.bool,
  renderHeader: PropTypes.func,
  renderBody: PropTypes.func,
};

export default AccordionComponent;
