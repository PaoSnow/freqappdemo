import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Button from '../Button/Button';
import ImageBackground from '../ImageBackground/ImageBackground';

class CardImage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {title, url} = this.props;
    return (
      <View>
        <ImageBackground
          url={url}
          imageStyle={{width: 500, height: 300}}
          textStyle={{top: 250, left: 20}}
          title={title}>
          <Button
            style={{
              width: 80,
              backgroundColor: 'transparent',
              borderRadius: 3,
              borderWidth: 1,
              borderColor: '#fff',
              height: 30,
              left: 280,
            }}
            value="Follow"
          />
        </ImageBackground>
      </View>
    );
  }
}

export default CardImage;
