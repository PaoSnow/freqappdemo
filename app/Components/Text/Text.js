import {ViewPropTypes, Text} from 'react-native';
import React, {Component} from 'react';
import PropTypes from 'prop-types';

const fontsMap = {
  small: {fontSize: 14},
  medium: {fontSize: 16},
  large: {fontSize: 28},
  xlarge: {fontSize: 36},
};

class TextComponent extends Component {
  getTextSize = () => {
    const {size} = this.props;

    return fontsMap[size] || fontsMap.medium;
  };

  getItalicStyle = () => {
    const {italic} = this.props;

    if (italic) {
      return {fontStyle: 'italic'};
    }
    return null;
  };

  getBoldStyle = () => {
    const {bold} = this.props;
    if (bold) {
      return {fontWeight: 'bold'};
    }
    return null;
  };

  render() {
    const {value, style, color} = this.props;

    return (
      <Text
        style={[
          {color},
          this.getTextSize(),
          this.getItalicStyle(),
          this.getBoldStyle(),
          style,
        ]}>
        {value}
      </Text>
    );
  }
}

export default TextComponent;

TextComponent.defaultProps = {
  value: null,
  style: null,
  size: 'medium',
  bold: false,
  italic: false,
  color: 'white',
};

TextComponent.propTypes = {
  value: PropTypes.string,
  style: ViewPropTypes.style,
  size: PropTypes.string,
  bold: PropTypes.bool,
  italic: PropTypes.bool,
  color: PropTypes.string,
};
