import React, {Component} from 'react';
import {View, TouchableOpacity} from 'react-native';
import TextComponent from '../Text/Text';

class TextButton extends Component {
  onPress = objItem => func => {
    const {onPress} = this.props;

    onPress(objItem);
  };

  render() {
    const {objValue, onPress, isSelected} = this.props;
    return (
      <TouchableOpacity onPress={this.onPress(objValue)}>
        <TextComponent
          value={objValue.value}
          bold={isSelected}
          {...this.props}
        />
      </TouchableOpacity>
    );
  }
}

export default TextButton;
