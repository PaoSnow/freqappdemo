import React, {Component} from 'react';
import {View, ImageBackground} from 'react-native';
import TextComponent from '../Text/Text';
import IconButton from '../IconButton/IconButton';

const TrackButton = props => {
  const {image, onPress} = props;

  return (
    <ImageBackground source={{uri: image}} style={{width: 60, height: 60}}>
      <View
        style={{alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
        <IconButton
          name="play-circle-o"
          color="white"
          size={40}
          onPress={onPress}
        />
      </View>
    </ImageBackground>
  );
};
export default TrackButton;
