import React, {Component} from 'react';
import {View} from 'react-native';
import AccordionComponent from '../Accordion/Accordion';
import Text from '../Text/Text';
import TextButton from '../TextButton/TextButton';
import IconComponent from '../Icon/Icon';

const FirstColumnMenuItems = [
  {
    id: 1,
    value: 'Channels and Sets',
  },
  {
    id: 2,
    value: 'Channels Only',
  },
  {
    id: 3,
    value: 'Sets Only',
  },
];

const SecondColumnMenuItems = [
  {
    id: 7,
    value: 'All',
  },
  {
    id: 8,
    value: 'DJs',
  },
  {
    id: 9,
    value: 'Labels',
  },
  {
    id: 10,
    value: 'Festivals',
  },
  {
    id: 11,
    value: 'Clubs',
  },
  {
    id: 12,
    value: 'Celebs Playlist',
  },
];

const ThirdColumnMenuItems = [
  {
    id: 13,
    value: 'Newest',
  },
  {
    id: 14,
    value: 'Most Popular',
  },
  {
    id: 15,
    value: 'A-Z',
  },
];

class MenuFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedItem: null,
    };
  }

  handleOnPress = objMenuItem => {
    this.setState({
      selectedItem: objMenuItem.id,
    });
  };

  renderHeader = textValue => {
    return (
      <View style={{flexDirection: 'row'}}>
        <Text value={textValue} size="small" style={{marginRight: 10}} />
        <IconComponent
          name="chevron-down"
          color="white"
          style={{marginTop: 3}}
        />
      </View>
    );
  };

  renderContent = listTextValue => {
    const {selectedItem} = this.state;

    return listTextValue.map(item => {
      return (
        <TextButton
          objValue={item}
          isSelected={item.id === selectedItem}
          onPress={this.handleOnPress}
          size="small"
          style={{margin: 10}}
        />
      );
    });
  };

  render() {
    return (
      <View
        style={{
          flex: 0.5,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View style={{flex: .4}}>
          <AccordionComponent
            renderHeader={this.renderHeader('Channels and Sets')}
            renderBody={this.renderContent(FirstColumnMenuItems)}
          />
        </View>
        <View style={{flex: .3, alignItems: 'center'}}>
          <AccordionComponent
            renderHeader={this.renderHeader('Show all')}
            renderBody={this.renderContent(SecondColumnMenuItems)}
          />
        </View>
        <View style={{flex: .3, alignItems: 'center'}}>
          <AccordionComponent
            renderHeader={this.renderHeader('Newest')}
            renderBody={this.renderContent(ThirdColumnMenuItems)}
          />
        </View>
      </View>
    );
  }
}
export default MenuFilter;
