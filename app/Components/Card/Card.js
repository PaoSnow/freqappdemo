import React, {Component} from 'react';
import {Image, View, StyleSheet} from 'react-native';
import Text from '../Text/Text';
import AccordionComponent from '../Accordion/Accordion';
import IconComponent from '../IconButton/IconButton';
import TrackButton from '../TrackButton/TrackButton';
import Icon from '../Icon/Icon';

class Card extends Component {
  renderHeader = () => {
    return (
      <Icon name="chevron-down" color="white" style={styles.icon} size={20} />
    );
  };

  renderBody = () => {
    const {paragraphText, subtTitle1Text, subtitle2Text} = this.props;

    return (
      <React.Fragment>
        <View>
          <Text style={styles.text} value={subtTitle1Text} />
          <Text value={subtitle2Text} />
          <Text style={styles.text} italic value={paragraphText} />
        </View>
        <View style={styles.footerContainer}>
          <IconComponent name="share-alt" color="white" size={20} />
          <IconComponent name="download" color="white" size={20} />
          <IconComponent name="heart" color="white" size={20} />
        </View>
      </React.Fragment>
    );
  };

  render() {
    const {
      image,
      actualTime,
      HeaderText,
      subHeaderText,
      initialValue,
    } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TrackButton image={image} actualTime={actualTime} />
          <View style={styles.headerText}>
            <Text value={HeaderText} size="large" color="white" />
            <Text value={subHeaderText} size="medium" />
          </View>
        </View>
        <View style={styles.bodyContainer}>
          <AccordionComponent
            initialValue={initialValue}
            renderHeader={this.renderHeader()}
            renderBody={this.renderBody()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
  },

  headerContainer: {
    flexDirection: 'row',
  },

  headerText: {
    margin: 5,
    justifyContent: 'space-between',
  },

  bodyContainer: {
    justifyContent: 'flex-end',
  },

  footerContainer: {
    marginLeft: 55,
    marginRight: 55,
    marginTop: 15,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  text: {
    marginTop: 10,
  },

  icon: {
    textAlign: 'right',
  },
});

export default Card;
