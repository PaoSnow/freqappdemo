import React, {Component} from 'react';;
import {View} from 'react-native';
import IconComponent from '../Icon/Icon';

class ProgressBar extends Component {
  render() {
    return <View style={{height: 2, backgroundColor: 'red', width: '90%'}} />;
  }
}

export default ProgressBar;
