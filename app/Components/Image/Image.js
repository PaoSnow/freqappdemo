import React, {Component} from 'react';
import {View, Image} from 'react-native';

class ImageComponent extends Component {
  render() {
    const {url, style, opacity} = this.props;
    return <Image source={{uri: url}} style={style} opacity={opacity} />;
  }
}

export default ImageComponent;
